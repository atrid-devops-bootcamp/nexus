#!/bin/bash

username=nom-user
password=kuthe8-farsyd-Jugxaq

apt update && apt install jq nodejs npm wget -y

# making a curl call on nexus and saving response on a json file
curl -u $username:$password -X GET 'http://167.71.46.143:8081/service/rest/v1/components?repository=npm&sort=version' | jq "." > artifact.json

# grab the download url from the saved artifact details using 'jq' json processor tool
artifactDownloadUrl=$(jq '.items[-1].assets[].downloadUrl' artifact.json --raw-output)

# getting file name from url
filename=$(jq '.items[-1].assets[].downloadUrl | split("/ *"; null)[-1]'  artifact.json --raw-output)

# fetch the artifact with the extracted download url using 'wget' tool
wget --http-user=$username --http-password=$password $artifactDownloadUrl

# untaring the file
tar zxvf $filename

cd /package && npm install && node server.js &
