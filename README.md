# 04 - Nexus

## 1. Introduction
This script is getting the information of the Nexus Repository Manager. It is using the Nexus API to get the information.
It gets the download url of the latest version. Performs a download of the latest version, untars it and starts the application.